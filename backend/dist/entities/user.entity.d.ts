import { Session } from './session.entity';
export declare class User {
    uuid: string;
    sessions: Session[];
}
