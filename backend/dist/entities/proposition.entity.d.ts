import { Session } from './session.entity';
import { User } from './user.entity';
import { Vote } from './vote.entity';
export declare class Proposition {
    uuid: string;
    text: string;
    totalAmount: number;
    session: Session;
    author: User;
    votes: Vote[];
}
