"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Proposition = void 0;
const typeorm_1 = require("typeorm");
const session_entity_1 = require("./session.entity");
const user_entity_1 = require("./user.entity");
const vote_entity_1 = require("./vote.entity");
let Proposition = class Proposition {
};
exports.Proposition = Proposition;
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], Proposition.prototype, "uuid", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], Proposition.prototype, "text", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: 0 }),
    __metadata("design:type", Number)
], Proposition.prototype, "totalAmount", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => session_entity_1.Session, (session) => session.propositions),
    __metadata("design:type", session_entity_1.Session)
], Proposition.prototype, "session", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => user_entity_1.User, { eager: true }),
    __metadata("design:type", user_entity_1.User)
], Proposition.prototype, "author", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => vote_entity_1.Vote, (vote) => vote.proposition, {
        eager: true,
        cascade: ['insert', 'update'],
    }),
    __metadata("design:type", Array)
], Proposition.prototype, "votes", void 0);
exports.Proposition = Proposition = __decorate([
    (0, typeorm_1.Entity)()
], Proposition);
//# sourceMappingURL=proposition.entity.js.map