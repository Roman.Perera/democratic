import { DatabaseService } from 'src/database/database.service';
import { ClassicVoteDto } from 'src/dto/classic-vote.dto';
import { InvestmentVoteDto } from 'src/dto/investment-vote.dto';
import { Session } from 'src/entities/session.entity';
import { Vote } from 'src/entities/vote.entity';
import { QueryRunnerFactory } from 'src/shared/query-runner-factory.service';
import { WebsocketGateway } from 'src/websocket/websocket.gateway';
import { EntityManager } from 'typeorm';
export declare class VoteService {
    private queryRunnerFactory;
    private databaseService;
    private readonly websocketGateway;
    constructor(queryRunnerFactory: QueryRunnerFactory, databaseService: DatabaseService, websocketGateway: WebsocketGateway);
    classicVote(dto: ClassicVoteDto): Promise<void>;
    investmentVote(dto: InvestmentVoteDto): Promise<void>;
    updateSessionState(votes: Vote[], entityManager: EntityManager, isInvestment?: boolean): Promise<void>;
    updatePropositionsTotalAmount(session: Session): void;
}
