import { VoteService } from './vote.service';
import { ClassicVoteDto } from 'src/dto/classic-vote.dto';
import { InvestmentVoteDto } from 'src/dto/investment-vote.dto';
export declare class VoteController {
    private readonly voteService;
    constructor(voteService: VoteService);
    classicVote(vote: ClassicVoteDto): Promise<void>;
    investmentVote(vote: InvestmentVoteDto): Promise<void>;
}
