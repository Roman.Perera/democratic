"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VoteService = void 0;
const common_1 = require("@nestjs/common");
const database_service_1 = require("../database/database.service");
const vote_entity_1 = require("../entities/vote.entity");
const query_runner_factory_service_1 = require("../shared/query-runner-factory.service");
const websocket_gateway_1 = require("../websocket/websocket.gateway");
let VoteService = class VoteService {
    constructor(queryRunnerFactory, databaseService, websocketGateway) {
        this.queryRunnerFactory = queryRunnerFactory;
        this.databaseService = databaseService;
        this.websocketGateway = websocketGateway;
    }
    async classicVote(dto) {
        await this.queryRunnerFactory.withTransaction(async (entityManager) => {
            const vote = new vote_entity_1.Vote();
            vote.proposition = { uuid: dto.propositionId };
            vote.amount = 1;
            vote.author = { uuid: dto.userId };
            await this.updateSessionState([vote], entityManager);
        });
    }
    async investmentVote(dto) {
        await this.queryRunnerFactory.withTransaction(async (entityManager) => {
            const votes = dto.votes.map((vote) => {
                const v = new vote_entity_1.Vote();
                v.proposition = { uuid: vote.propositionId };
                v.amount = vote.amount;
                v.author = { uuid: vote.userId };
                return v;
            });
            await this.updateSessionState(votes, entityManager, true);
        });
    }
    async updateSessionState(votes, entityManager, isInvestment = false) {
        const session = await this.databaseService.getSessionFromPropositionId(votes[0].proposition.uuid);
        console.log(session);
        if (isInvestment && session.type === 'classic') {
            throw new Error('Investment vote on classic session');
        }
        for (const vote of votes) {
            const proposition = session.propositions.find((p) => p.uuid === vote.proposition.uuid);
            if (!proposition) {
                throw new Error('Proposition not found');
            }
            proposition.votes.push(vote);
        }
        session.nUserFinishedVoting += 1;
        let isNextStep = false;
        if (session.nUserFinishedVoting === session.connectedUsers) {
            session.state = 'finished';
            session.active = false;
            this.updatePropositionsTotalAmount(session);
            isNextStep = true;
        }
        await this.databaseService.saveSession(session, entityManager);
        this.websocketGateway.handleWebsocketEvent(isNextStep, session);
    }
    updatePropositionsTotalAmount(session) {
        session.propositions.forEach((proposition) => {
            proposition.totalAmount = proposition.votes.reduce((acc, vote) => acc + vote.amount, 0);
        });
    }
};
exports.VoteService = VoteService;
exports.VoteService = VoteService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [query_runner_factory_service_1.QueryRunnerFactory,
        database_service_1.DatabaseService,
        websocket_gateway_1.WebsocketGateway])
], VoteService);
//# sourceMappingURL=vote.service.js.map