"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatabaseService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const proposition_entity_1 = require("../entities/proposition.entity");
const session_entity_1 = require("../entities/session.entity");
const user_entity_1 = require("../entities/user.entity");
const typeorm_2 = require("typeorm");
let DatabaseService = class DatabaseService {
    constructor(sessionRepository, userRepository, propositionRepository) {
        this.sessionRepository = sessionRepository;
        this.userRepository = userRepository;
        this.propositionRepository = propositionRepository;
    }
    async saveSession(session, entityManager) {
        return entityManager.save(session);
    }
    async updateSession(session, entityManager) {
        await entityManager.update(session_entity_1.Session, session.uuid, session);
    }
    async findActiveSession(pinCode) {
        return this.sessionRepository.findOne({
            where: { pinCode: pinCode, active: true },
            relations: ['propositions'],
        });
    }
    async findUserByUuid(userId) {
        return this.userRepository.findOne({ where: { uuid: userId } });
    }
    async getUserActiveSessions(userId) {
        return this.userRepository
            .createQueryBuilder('user')
            .leftJoinAndSelect('user.sessions', 'session')
            .leftJoinAndSelect('session.propositions', 'proposition')
            .leftJoinAndSelect('proposition.votes', 'vote')
            .leftJoinAndSelect('vote.author', 'voteAuthor')
            .leftJoinAndSelect('proposition.author', 'author')
            .leftJoinAndSelect('session.sessionManager', 'sessionManager')
            .where('user.uuid = :userId', { userId })
            .andWhere('session.active = true')
            .getMany()
            .then((users) => users.flatMap((user) => user.sessions));
    }
    async findSessionByUuidWithoutRelations(uuid) {
        return this.sessionRepository.findOne({
            where: { uuid: uuid },
        });
    }
    async findSessionByUuid(uuid) {
        return this.sessionRepository.findOne({
            where: { uuid: uuid },
            relations: ['propositions'],
        });
    }
    async saveUser(user) {
        return this.userRepository.save(user);
    }
    async saveClassicVote(vote, entityManager) {
        return entityManager.save(vote);
    }
    async saveInvestmentVote(votes, entityManager) {
        return entityManager.save(votes);
    }
    async savePropositions(propositions, entityManager) {
        return entityManager.save(propositions);
    }
    async getSessionFromPropositionId(propositionId) {
        const proposition = await this.propositionRepository.findOne({
            where: { uuid: propositionId },
            relations: ['session', 'session.propositions'],
        });
        if (!proposition) {
            throw new Error('Proposition not found');
        }
        return proposition.session;
    }
    async addSessionToUser(userId, session, entityManager) {
        const user = await this.userRepository.findOne({ where: { uuid: userId } });
        if (!user) {
            throw new Error('User not found');
        }
        await entityManager
            .createQueryBuilder()
            .relation(user_entity_1.User, 'sessions')
            .of(user)
            .add(session);
    }
};
exports.DatabaseService = DatabaseService;
exports.DatabaseService = DatabaseService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(session_entity_1.Session)),
    __param(1, (0, typeorm_1.InjectRepository)(user_entity_1.User)),
    __param(2, (0, typeorm_1.InjectRepository)(proposition_entity_1.Proposition)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], DatabaseService);
//# sourceMappingURL=database.service.js.map