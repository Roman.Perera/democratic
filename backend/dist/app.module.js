"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const websocket_module_1 = require("./websocket/websocket.module");
const query_runner_factory_service_1 = require("./shared/query-runner-factory.service");
const shared_module_1 = require("./shared/shared.module");
const database_module_1 = require("./database/database.module");
const common_1 = require("@nestjs/common");
const session_module_1 = require("./session/session.module");
const proposition_module_1 = require("./proposition/proposition.module");
const vote_module_1 = require("./vote/vote.module");
const typeorm_1 = require("@nestjs/typeorm");
const throttler_1 = require("@nestjs/throttler");
const core_1 = require("@nestjs/core");
let AppModule = class AppModule {
};
exports.AppModule = AppModule;
exports.AppModule = AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            websocket_module_1.WebsocketModule,
            shared_module_1.SharedModule,
            database_module_1.DatabaseModule,
            session_module_1.SessionModule,
            proposition_module_1.PropositionModule,
            vote_module_1.VoteModule,
            throttler_1.ThrottlerModule.forRoot([
                {
                    ttl: 60,
                    limit: 10,
                },
            ]),
            typeorm_1.TypeOrmModule.forRoot({
                type: 'mariadb',
                host: '0.0.0.0',
                port: 3306,
                username: 'root',
                password: '',
                database: 'democratic',
                autoLoadEntities: true,
                synchronize: true,
                dropSchema: false,
                logging: true,
            }),
        ],
        providers: [
            {
                provide: core_1.APP_GUARD,
                useClass: throttler_1.ThrottlerGuard,
            },
            query_runner_factory_service_1.QueryRunnerFactory,
        ],
    })
], AppModule);
//# sourceMappingURL=app.module.js.map