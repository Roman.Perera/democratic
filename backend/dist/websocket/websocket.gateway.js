"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebsocketGateway = void 0;
const websockets_1 = require("@nestjs/websockets");
const socket_io_1 = require("socket.io");
const database_service_1 = require("../database/database.service");
const navigation_dto_1 = require("../dto/navigation.dto");
const session_state_update_dto_1 = require("../dto/session-state-update.dto");
const user_entity_1 = require("../entities/user.entity");
let WebsocketGateway = class WebsocketGateway {
    constructor(databaseService) {
        this.databaseService = databaseService;
    }
    handleEvent(data) {
        this.server.emit('events', data);
    }
    async handleConnection(client) {
        const userId = client.handshake.query.userId;
        console.log(client.handshake.query.userId);
        client.join(userId);
        await this.checkActiveSession(client, userId);
    }
    async checkActiveSession(client, userId) {
        try {
            const user = await this.databaseService.findUserByUuid(userId);
            if (!user) {
                const user = new user_entity_1.User();
                user.uuid = userId;
                await this.databaseService.saveUser(user);
            }
            else {
                const activeSessions = await this.databaseService.getUserActiveSessions(userId);
                console.log('activeSessions', activeSessions);
                if (activeSessions.length > 0) {
                    client.join(activeSessions[0].uuid);
                    const dto = new navigation_dto_1.NavigationDto(activeSessions[0]);
                    dto.setPageForReconnectedUser(userId);
                    console.log('NavigationSocketDto', JSON.stringify(dto));
                    client.emit('navigation', JSON.stringify(dto));
                }
            }
        }
        catch (error) {
            console.log(error);
        }
    }
    sendActiveSessionToUser(session, userId) {
        const response = new navigation_dto_1.NavigationDto(session);
        response.setPageForReconnectedUser(userId);
        this.server.to(userId).emit('navigation', JSON.stringify(response));
    }
    joinToSessionRoom(sessionUuid, userId) {
        const client = this.server.sockets.adapter.rooms.get(userId);
        client.forEach((socketId) => {
            this.server.sockets.sockets.get(socketId).join(sessionUuid);
        });
    }
    sendNextStepEvent(session) {
        const dto = new navigation_dto_1.NavigationDto(session);
        dto.setPageForNextStep();
        this.server.to(session.uuid).emit('navigation', JSON.stringify(dto));
    }
    sendSessionStateUpdate(session) {
        const dto = new session_state_update_dto_1.SessionStateUpdate(session);
        console.log('SessionStateUpdateDto', JSON.stringify(dto));
        this.server
            .to(session.uuid)
            .emit('updateSessionStateInfo', JSON.stringify(dto));
    }
    handleWebsocketEvent(isNextStep, session) {
        if (isNextStep) {
            this.sendNextStepEvent(session);
        }
        else {
            this.sendSessionStateUpdate(session);
        }
    }
    handleDisconnect(client) {
        console.log('User disconnected');
    }
    afterInit(server) {
        console.log('Socket is live');
    }
};
exports.WebsocketGateway = WebsocketGateway;
__decorate([
    (0, websockets_1.WebSocketServer)(),
    __metadata("design:type", socket_io_1.Server)
], WebsocketGateway.prototype, "server", void 0);
__decorate([
    (0, websockets_1.SubscribeMessage)('events'),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], WebsocketGateway.prototype, "handleEvent", null);
exports.WebsocketGateway = WebsocketGateway = __decorate([
    (0, websockets_1.WebSocketGateway)(),
    __metadata("design:paramtypes", [database_service_1.DatabaseService])
], WebsocketGateway);
//# sourceMappingURL=websocket.gateway.js.map