import { OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { DatabaseService } from 'src/database/database.service';
import { Session } from 'src/entities/session.entity';
export declare class WebsocketGateway implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {
    private readonly databaseService;
    constructor(databaseService: DatabaseService);
    server: Server;
    handleEvent(data: string): void;
    handleConnection(client: Socket): Promise<void>;
    checkActiveSession(client: Socket, userId: string): Promise<void>;
    sendActiveSessionToUser(session: Session, userId: string): void;
    joinToSessionRoom(sessionUuid: string, userId: string): void;
    sendNextStepEvent(session: Session): void;
    sendSessionStateUpdate(session: Session): void;
    handleWebsocketEvent(isNextStep: boolean, session: Session): void;
    handleDisconnect(client: any): void;
    afterInit(server: any): void;
}
