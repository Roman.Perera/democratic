"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueryRunnerFactory = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
let QueryRunnerFactory = class QueryRunnerFactory {
    constructor(dataSource) {
        this.dataSource = dataSource;
    }
    async withTransaction(work) {
        const queryRunner = this.dataSource.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            const result = await work(queryRunner.manager);
            await queryRunner.commitTransaction();
            return result;
        }
        catch (err) {
            await queryRunner.rollbackTransaction();
            console.log(err);
            throw err;
        }
        finally {
            await queryRunner.release();
        }
    }
};
exports.QueryRunnerFactory = QueryRunnerFactory;
exports.QueryRunnerFactory = QueryRunnerFactory = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [typeorm_1.DataSource])
], QueryRunnerFactory);
//# sourceMappingURL=query-runner-factory.service.js.map