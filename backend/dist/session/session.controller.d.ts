import { SessionService } from './session.service';
import { CreateSessionDto } from 'src/dto/create-session.dto';
import { FindSessionWithPinCodeDto } from 'src/dto/pin-code.dto';
import { UuidDto } from 'src/dto/uuid.dto';
export declare class SessionController {
    private readonly sessionService;
    constructor(sessionService: SessionService);
    create(createSessionDto: CreateSessionDto): Promise<import("../entities/session.entity").Session>;
    findFromPin(dto: FindSessionWithPinCodeDto): Promise<boolean>;
    nextStep(uuid: UuidDto): Promise<void>;
}
