"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SessionService = void 0;
const common_1 = require("@nestjs/common");
const database_service_1 = require("../database/database.service");
const query_runner_factory_service_1 = require("../shared/query-runner-factory.service");
const session_entity_1 = require("../entities/session.entity");
const websocket_gateway_1 = require("../websocket/websocket.gateway");
let SessionService = class SessionService {
    constructor(queryRunnerFactory, databaseService, websocketGateway) {
        this.queryRunnerFactory = queryRunnerFactory;
        this.databaseService = databaseService;
        this.websocketGateway = websocketGateway;
    }
    async create(createSessionDto) {
        const pinCode = await this.findNewPinCode();
        return await this.queryRunnerFactory.withTransaction(async (entityManager) => {
            let session = new session_entity_1.Session();
            session.pinCode = pinCode;
            session.multiplePropositions =
                createSessionDto.isMultiplePropositionsAllowed;
            session.problematic = createSessionDto.problematic;
            session.type = createSessionDto.sessionType;
            session.fixedSize = createSessionDto.isGroupSizeDefined;
            session.size = createSessionDto.groupSize;
            session.connectedUsers = 1;
            session.sessionManager = {
                uuid: createSessionDto.sessionManagerId,
            };
            session.propositions = [];
            session = await this.databaseService.saveSession(session, entityManager);
            await this.databaseService.addSessionToUser(createSessionDto.sessionManagerId, session, entityManager);
            this.websocketGateway.joinToSessionRoom(session.uuid, createSessionDto.sessionManagerId);
            return session;
        });
    }
    async findActiveSession(dto) {
        const session = await this.databaseService.findActiveSession(dto.pinCode);
        if (!session) {
            return false;
        }
        await this.queryRunnerFactory.withTransaction(async (entityManager) => {
            session.connectedUsers += 1;
            await this.databaseService.saveSession(session, entityManager);
            await this.databaseService.addSessionToUser(dto.userId, session, entityManager);
            this.websocketGateway.sendSessionStateUpdate(session);
            this.websocketGateway.sendActiveSessionToUser(session, dto.userId);
            this.websocketGateway.joinToSessionRoom(session.uuid, dto.userId);
        });
        return true;
    }
    async findNewPinCode() {
        const pinCode = Math.floor(1 + Math.random() * 9999)
            .toString()
            .padStart(4, '0');
        const session = await this.databaseService.findActiveSession(pinCode);
        if (session) {
            return this.findNewPinCode();
        }
        return pinCode;
    }
    async nextStep(dto) {
        await this.queryRunnerFactory.withTransaction(async (entityManager) => {
            const session = await this.databaseService.findSessionByUuid(dto.uuid);
            if (!session) {
                throw new Error('Session not found');
            }
            if (session.state === 'proposing') {
                session.state = 'voting';
            }
            else if (session.state === 'voting') {
                session.state = 'finished';
                session.active = false;
            }
            await this.databaseService.saveSession(session, entityManager);
            this.websocketGateway.sendNextStepEvent(session);
        });
    }
};
exports.SessionService = SessionService;
exports.SessionService = SessionService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [query_runner_factory_service_1.QueryRunnerFactory,
        database_service_1.DatabaseService,
        websocket_gateway_1.WebsocketGateway])
], SessionService);
//# sourceMappingURL=session.service.js.map