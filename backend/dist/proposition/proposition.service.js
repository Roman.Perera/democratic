"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PropositionService = void 0;
const common_1 = require("@nestjs/common");
const database_service_1 = require("../database/database.service");
const proposition_entity_1 = require("../entities/proposition.entity");
const query_runner_factory_service_1 = require("../shared/query-runner-factory.service");
const websocket_gateway_1 = require("../websocket/websocket.gateway");
let PropositionService = class PropositionService {
    constructor(queryRunnerFactory, databaseService, websocketGateway) {
        this.queryRunnerFactory = queryRunnerFactory;
        this.databaseService = databaseService;
        this.websocketGateway = websocketGateway;
    }
    async createProposition(dto) {
        const session = await this.databaseService.findSessionByUuidWithoutRelations(dto.sessionId);
        await this.queryRunnerFactory.withTransaction(async (queryRunner) => {
            if (!session.multiplePropositions) {
                dto.propositions = dto.propositions.slice(0, 1);
            }
            const propositions = dto.propositions.map((proposition) => {
                const propositionEntity = new proposition_entity_1.Proposition();
                propositionEntity.text = proposition;
                propositionEntity.session = { uuid: dto.sessionId };
                propositionEntity.author = { uuid: dto.userId };
                propositionEntity.totalAmount = 0;
                return propositionEntity;
            });
            await this.databaseService.savePropositions(propositions, queryRunner);
            await this.updateSessionState(session, queryRunner);
        });
    }
    async updateSessionState(session, entityManager) {
        session.nUserFinishedProposing += 1;
        let isNextStep = false;
        if (session.fixedSize && session.nUserFinishedProposing === session.size) {
            session.state = 'voting';
            isNextStep = true;
        }
        await this.databaseService.updateSession(session, entityManager);
        this.websocketGateway.handleWebsocketEvent(isNextStep, session);
    }
};
exports.PropositionService = PropositionService;
exports.PropositionService = PropositionService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [query_runner_factory_service_1.QueryRunnerFactory,
        database_service_1.DatabaseService,
        websocket_gateway_1.WebsocketGateway])
], PropositionService);
//# sourceMappingURL=proposition.service.js.map