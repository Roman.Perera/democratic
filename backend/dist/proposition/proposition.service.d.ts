import { DatabaseService } from 'src/database/database.service';
import { CreatePropositionsDto } from 'src/dto/create-propositions.dto';
import { Session } from 'src/entities/session.entity';
import { QueryRunnerFactory } from 'src/shared/query-runner-factory.service';
import { WebsocketGateway } from 'src/websocket/websocket.gateway';
import { EntityManager } from 'typeorm';
export declare class PropositionService {
    private queryRunnerFactory;
    private databaseService;
    private readonly websocketGateway;
    constructor(queryRunnerFactory: QueryRunnerFactory, databaseService: DatabaseService, websocketGateway: WebsocketGateway);
    createProposition(dto: CreatePropositionsDto): Promise<void>;
    updateSessionState(session: Session, entityManager: EntityManager): Promise<void>;
}
