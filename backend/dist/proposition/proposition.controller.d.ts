import { PropositionService } from './proposition.service';
import { CreatePropositionsDto } from 'src/dto/create-propositions.dto';
export declare class PropositionController {
    private readonly propositionService;
    constructor(propositionService: PropositionService);
    createProposition(dto: CreatePropositionsDto): Promise<void>;
}
