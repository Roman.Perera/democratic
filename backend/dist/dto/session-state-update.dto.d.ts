import { Session } from 'src/entities/session.entity';
export declare class SessionStateUpdate {
    state: string;
    connectedUsers: number;
    nUserFinishedProposing: number;
    nUserFinishedVoting: number;
    constructor(session: Session);
}
