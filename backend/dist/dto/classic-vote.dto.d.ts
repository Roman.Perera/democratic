export declare class ClassicVoteDto {
    propositionId: string;
    userId: string;
}
