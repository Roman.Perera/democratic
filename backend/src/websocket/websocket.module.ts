/*
https://docs.nestjs.com/modules
*/

import { Module } from '@nestjs/common';
import { WebsocketGateway } from './websocket.gateway';
import { DatabaseModule } from 'src/database/database.module';

@Module({
  imports: [DatabaseModule],
  providers: [WebsocketGateway],
  exports: [WebsocketGateway],
})
export class WebsocketModule {}
