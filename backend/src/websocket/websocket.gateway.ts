/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
} from '@nestjs/websockets';
import { join } from 'node:path';
import { Server, Socket } from 'socket.io';
import { DatabaseService } from 'src/database/database.service';
import { NavigationDto as NavigationDto } from 'src/dto/navigation.dto';
import { SessionStateUpdate as SessionStateUpdateDto } from 'src/dto/session-state-update.dto';
import { Session } from 'src/entities/session.entity';
import { User } from 'src/entities/user.entity';

@WebSocketGateway()
export class WebsocketGateway
  implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit
{
  constructor(private readonly databaseService: DatabaseService) {}
  @WebSocketServer()
  server: Server;

  @SubscribeMessage('events')
  handleEvent(@MessageBody() data: string) {
    this.server.emit('events', data);
  }

  async handleConnection(client: Socket) {
    const userId: string = client.handshake.query.userId as string;
    console.log(client.handshake.query.userId);
    client.join(userId);
    await this.checkActiveSession(client, userId);
  }

  async checkActiveSession(client: Socket, userId: string) {
    try {
      const user = await this.databaseService.findUserByUuid(userId);
      if (!user) {
        const user = new User();
        user.uuid = userId;
        await this.databaseService.saveUser(user);
      } else {
        const activeSessions =
          await this.databaseService.getUserActiveSessions(userId);
        console.log('activeSessions', activeSessions);
        if (activeSessions.length > 0) {
          client.join(activeSessions[0].uuid); // Peut pas avoir plusieurs sessions actives...
          const dto = new NavigationDto(activeSessions[0]);
          dto.setPageForReconnectedUser(userId);
          console.log('NavigationSocketDto', JSON.stringify(dto));
          client.emit('navigation', JSON.stringify(dto));
        }
      }
    } catch (error) {
      //TODO: Log error
      console.log(error);
    }
  }

  sendActiveSessionToUser(session: Session, userId: string) {
    const response = new NavigationDto(session);
    response.setPageForReconnectedUser(userId);
    this.server.to(userId).emit('navigation', JSON.stringify(response));
  }

  joinToSessionRoom(sessionUuid: string, userId: string) {
    const client = this.server.sockets.adapter.rooms.get(userId);
    client.forEach((socketId) => {
      this.server.sockets.sockets.get(socketId).join(sessionUuid);
    });
  }

  sendNextStepEvent(session: Session) {
    const dto = new NavigationDto(session);
    dto.setPageForNextStep();
    this.server.to(session.uuid).emit('navigation', JSON.stringify(dto));
  }

  sendSessionStateUpdate(session: Session) {
    const dto = new SessionStateUpdateDto(session);
    console.log('SessionStateUpdateDto', JSON.stringify(dto));
    this.server
      .to(session.uuid)
      .emit('updateSessionStateInfo', JSON.stringify(dto));
  }

  handleWebsocketEvent(isNextStep: boolean, session: Session) {
    if (isNextStep) {
      this.sendNextStepEvent(session);
    } else {
      this.sendSessionStateUpdate(session);
    }
  }

  handleDisconnect(client: any) {
    console.log('User disconnected');
  }

  afterInit(server: any) {
    console.log('Socket is live');
  }
}
