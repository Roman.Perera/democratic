import { Injectable } from '@nestjs/common';
import { DatabaseService } from 'src/database/database.service';
import { QueryRunnerFactory } from 'src/shared/query-runner-factory.service';
import { CreateSessionDto } from 'src/dto/create-session.dto';
import { Session } from 'src/entities/session.entity';
import { EntityManager } from 'typeorm';
import { User } from 'src/entities/user.entity';
import { FindSessionWithPinCodeDto } from 'src/dto/pin-code.dto';
import { UuidDto } from 'src/dto/uuid.dto';
import { WebsocketGateway } from 'src/websocket/websocket.gateway';

@Injectable()
export class SessionService {
  constructor(
    private queryRunnerFactory: QueryRunnerFactory,
    private databaseService: DatabaseService,
    private readonly websocketGateway: WebsocketGateway,
  ) {}

  async create(createSessionDto: CreateSessionDto) {
    const pinCode = await this.findNewPinCode();

    return await this.queryRunnerFactory.withTransaction(
      async (entityManager: EntityManager) => {
        let session: Session = new Session();
        session.pinCode = pinCode;
        session.multiplePropositions =
          createSessionDto.isMultiplePropositionsAllowed;
        session.problematic = createSessionDto.problematic;
        session.type = createSessionDto.sessionType;
        session.fixedSize = createSessionDto.isGroupSizeDefined;
        session.size = createSessionDto.groupSize;
        session.connectedUsers = 1;
        session.sessionManager = {
          uuid: createSessionDto.sessionManagerId,
        } as User;
        session.propositions = [];
        session = await this.databaseService.saveSession(
          session,
          entityManager,
        );
        await this.databaseService.addSessionToUser(
          createSessionDto.sessionManagerId,
          session,
          entityManager,
        );
        this.websocketGateway.joinToSessionRoom(
          session.uuid,
          createSessionDto.sessionManagerId,
        );
        return session;
      },
    );
  }

  async findActiveSession(dto: FindSessionWithPinCodeDto): Promise<boolean> {
    const session = await this.databaseService.findActiveSession(dto.pinCode);

    if (!session) {
      return false;
    }

    await this.queryRunnerFactory.withTransaction(
      async (entityManager: EntityManager) => {
        session.connectedUsers += 1;

        await this.databaseService.saveSession(session, entityManager);
        await this.databaseService.addSessionToUser(
          dto.userId,
          session,
          entityManager,
        );
        this.websocketGateway.sendSessionStateUpdate(session);
        this.websocketGateway.sendActiveSessionToUser(session, dto.userId);
        this.websocketGateway.joinToSessionRoom(session.uuid, dto.userId);
      },
    );

    return true;
  }

  async findNewPinCode(): Promise<string> {
    const pinCode: string = Math.floor(1 + Math.random() * 9999)
      .toString()
      .padStart(4, '0');

    const session = await this.databaseService.findActiveSession(pinCode);

    if (session) {
      return this.findNewPinCode();
    }

    return pinCode;
  }

  async nextStep(dto: UuidDto) {
    await this.queryRunnerFactory.withTransaction(
      async (entityManager: EntityManager) => {
        const session = await this.databaseService.findSessionByUuid(dto.uuid);

        if (!session) {
          throw new Error('Session not found');
        }

        if (session.state === 'proposing') {
          session.state = 'voting';
        } else if (session.state === 'voting') {
          session.state = 'finished';
          session.active = false;
        }

        await this.databaseService.saveSession(session, entityManager);
        this.websocketGateway.sendNextStepEvent(session);
      },
    );
  }
}
