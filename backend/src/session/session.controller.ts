import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { SessionService } from './session.service';
import { CreateSessionDto } from 'src/dto/create-session.dto';
import { FindSessionWithPinCodeDto } from 'src/dto/pin-code.dto';
import { UuidDto } from 'src/dto/uuid.dto';
// import { TestDto } from 'src/dto/test.dto';

@Controller('session')
export class SessionController {
  constructor(private readonly sessionService: SessionService) {}

  @Post()
  async create(@Body() createSessionDto: CreateSessionDto) {
    console.log('AAAAAAAAAAAAAAAASDS');
    return await this.sessionService.create(createSessionDto);
  }

  @Get('pin/:pinCode/:userId')
  async findFromPin(@Param() dto: FindSessionWithPinCodeDto) {
    console.log('dto', dto);
    return this.sessionService.findActiveSession(dto);
  }

  @Post('next/:uuid')
  async nextStep(@Param() uuid: UuidDto) {
    return this.sessionService.nextStep(uuid);
  }
}
