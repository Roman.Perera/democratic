import { Module } from '@nestjs/common';
import { SessionService } from './session.service';
import { SessionController } from './session.controller';
import { SharedModule } from 'src/shared/shared.module';
import { DatabaseModule } from 'src/database/database.module';
import { WebsocketModule } from 'src/websocket/websocket.module';

@Module({
  imports: [SharedModule, DatabaseModule, WebsocketModule],
  providers: [SessionService],
  controllers: [SessionController],
})
export class SessionModule {}
