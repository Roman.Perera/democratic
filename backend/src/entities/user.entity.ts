import { Entity, PrimaryColumn, ManyToMany, JoinTable } from 'typeorm';
import { Session } from './session.entity';

@Entity()
export class User {
  @PrimaryColumn()
  uuid: string;

  @ManyToMany(() => Session)
  @JoinTable()
  sessions: Session[];
}
