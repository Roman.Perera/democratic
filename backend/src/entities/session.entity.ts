import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { Proposition } from './proposition.entity';
import { User } from './user.entity';

@Entity()
export class Session {
  @PrimaryGeneratedColumn('uuid')
  uuid: string;

  @Column()
  pinCode: string;

  @Column({ default: false })
  multiplePropositions: boolean;

  @Column({ default: false })
  fixedSize: boolean;

  @Column({ nullable: true })
  size: number;

  @Column({
    type: 'enum',
    enum: ['proposing', 'voting', 'cancelled', 'finished'],
    default: 'proposing',
  })
  state: string;

  @Column({ default: true })
  active: boolean;

  @Column({ default: 0 })
  connectedUsers: number;

  @Column({ default: 0 })
  nUserFinishedProposing: number;

  @Column({ default: 0 })
  nUserFinishedVoting: number;

  @Column({ type: 'enum', enum: ['classic', 'investment'], default: 'classic' })
  type: string;

  @Column({ nullable: true })
  problematic: string;

  @OneToMany(() => Proposition, (proposition) => proposition.session, {
    eager: false,
    cascade: ['update'],
  })
  propositions: Proposition[];

  @ManyToOne(() => User, { eager: true })
  sessionManager: User;
}
