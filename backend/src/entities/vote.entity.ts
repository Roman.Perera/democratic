import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Proposition } from './proposition.entity';
import { User } from './user.entity';

@Entity()
export class Vote {
  @PrimaryGeneratedColumn('uuid')
  uuid: string;

  @Column()
  amount: number;

  @ManyToOne(() => Proposition, { eager: false })
  proposition: Proposition;

  @ManyToOne(() => User)
  author: User;
}
