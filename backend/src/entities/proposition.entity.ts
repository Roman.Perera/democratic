import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Session } from './session.entity';
import { User } from './user.entity';
import { Vote } from './vote.entity';

@Entity()
export class Proposition {
  @PrimaryGeneratedColumn('uuid')
  uuid: string;

  @Column()
  text: string;

  @Column({ default: 0 })
  totalAmount: number;

  @ManyToOne(() => Session, (session) => session.propositions)
  session: Session;

  @ManyToOne(() => User, { eager: true })
  author: User;

  @OneToMany(() => Vote, (vote) => vote.proposition, {
    eager: true,
    cascade: ['insert', 'update'],
  })
  votes: Vote[];
}
