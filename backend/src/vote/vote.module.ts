import { Module } from '@nestjs/common';
import { VoteService } from './vote.service';
import { VoteController } from './vote.controller';
import { SharedModule } from 'src/shared/shared.module';
import { DatabaseModule } from 'src/database/database.module';
import { WebsocketModule } from 'src/websocket/websocket.module';

@Module({
  imports: [SharedModule, DatabaseModule, WebsocketModule],
  providers: [VoteService],
  controllers: [VoteController],
})
export class VoteModule {}
