import { Injectable } from '@nestjs/common';
import { DatabaseService } from 'src/database/database.service';
import { ClassicVoteDto } from 'src/dto/classic-vote.dto';
import { InvestmentVoteDto } from 'src/dto/investment-vote.dto';
import { Proposition } from 'src/entities/proposition.entity';
import { Session } from 'src/entities/session.entity';
import { User } from 'src/entities/user.entity';
import { Vote } from 'src/entities/vote.entity';
import { QueryRunnerFactory } from 'src/shared/query-runner-factory.service';
import { WebsocketGateway } from 'src/websocket/websocket.gateway';
import { EntityManager } from 'typeorm';

@Injectable()
export class VoteService {
  constructor(
    private queryRunnerFactory: QueryRunnerFactory,
    private databaseService: DatabaseService,
    private readonly websocketGateway: WebsocketGateway,
  ) {}

  async classicVote(dto: ClassicVoteDto) {
    await this.queryRunnerFactory.withTransaction(async (entityManager) => {
      const vote: Vote = new Vote();
      vote.proposition = { uuid: dto.propositionId } as Proposition;
      vote.amount = 1;
      vote.author = { uuid: dto.userId } as User;
      await this.updateSessionState([vote], entityManager);
    });
  }

  async investmentVote(dto: InvestmentVoteDto) {
    await this.queryRunnerFactory.withTransaction(async (entityManager) => {
      const votes: Vote[] = dto.votes.map((vote) => {
        const v: Vote = new Vote();
        v.proposition = { uuid: vote.propositionId } as Proposition;
        v.amount = vote.amount;
        v.author = { uuid: vote.userId } as User;
        return v;
      });
      await this.updateSessionState(votes, entityManager, true);
    });
  }

  async updateSessionState(
    votes: Vote[],
    entityManager: EntityManager,
    isInvestment: boolean = false,
  ) {
    const session = await this.databaseService.getSessionFromPropositionId(
      votes[0].proposition.uuid,
    );
    console.log(session);
    if (isInvestment && session.type === 'classic') {
      throw new Error('Investment vote on classic session');
    }

    for (const vote of votes) {
      //find the right proposition in the session.propositions array
      const proposition = session.propositions.find(
        (p) => p.uuid === vote.proposition.uuid,
      );
      if (!proposition) {
        throw new Error('Proposition not found');
      }
      proposition.votes.push(vote);
    }

    session.nUserFinishedVoting += 1;
    let isNextStep = false;

    if (session.nUserFinishedVoting === session.connectedUsers) {
      session.state = 'finished';
      session.active = false;
      this.updatePropositionsTotalAmount(session);
      isNextStep = true;
    }

    await this.databaseService.saveSession(session, entityManager);
    this.websocketGateway.handleWebsocketEvent(isNextStep, session);
  }

  updatePropositionsTotalAmount(session: Session) {
    session.propositions.forEach((proposition) => {
      proposition.totalAmount = proposition.votes.reduce(
        (acc, vote) => acc + vote.amount,
        0,
      );
    });
  }
}
