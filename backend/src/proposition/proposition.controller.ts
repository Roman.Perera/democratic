import { Body, Controller, Post } from '@nestjs/common';
import { PropositionService } from './proposition.service';
import { CreatePropositionsDto } from 'src/dto/create-propositions.dto';

@Controller('proposition')
export class PropositionController {
  constructor(private readonly propositionService: PropositionService) {}

  @Post()
  async createProposition(@Body() dto: CreatePropositionsDto) {
    return this.propositionService.createProposition(dto);
  }
}
