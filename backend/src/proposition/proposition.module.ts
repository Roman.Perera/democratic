import { Module } from '@nestjs/common';
import { PropositionService } from './proposition.service';
import { PropositionController } from './proposition.controller';
import { SharedModule } from 'src/shared/shared.module';
import { DatabaseModule } from 'src/database/database.module';
import { WebsocketModule } from 'src/websocket/websocket.module';

@Module({
  imports: [SharedModule, DatabaseModule, WebsocketModule],
  providers: [PropositionService],
  controllers: [PropositionController],
})
export class PropositionModule {}
