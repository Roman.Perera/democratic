import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Proposition } from 'src/entities/proposition.entity';
import { Session } from 'src/entities/session.entity';
import { User } from 'src/entities/user.entity';
import { Vote } from 'src/entities/vote.entity';
import { EntityManager, Repository } from 'typeorm';
@Injectable()
export class DatabaseService {
  constructor(
    @InjectRepository(Session)
    private sessionRepository: Repository<Session>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Proposition)
    private propositionRepository: Repository<Proposition>,
  ) {}

  async saveSession(
    session: Session,
    entityManager: EntityManager,
  ): Promise<Session> {
    return entityManager.save(session);
  }

  async updateSession(
    session: Session,
    entityManager: EntityManager,
  ): Promise<void> {
    await entityManager.update(Session, session.uuid, session);
  }

  async findActiveSession(pinCode: string): Promise<Session> {
    return this.sessionRepository.findOne({
      where: { pinCode: pinCode, active: true },
      relations: ['propositions'],
    });
  }

  async findUserByUuid(userId: string): Promise<User> {
    return this.userRepository.findOne({ where: { uuid: userId } });
  }

  async getUserActiveSessions(userId: string): Promise<Session[]> {
    return this.userRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.sessions', 'session')
      .leftJoinAndSelect('session.propositions', 'proposition')
      .leftJoinAndSelect('proposition.votes', 'vote')
      .leftJoinAndSelect('vote.author', 'voteAuthor')
      .leftJoinAndSelect('proposition.author', 'author')
      .leftJoinAndSelect('session.sessionManager', 'sessionManager')
      .where('user.uuid = :userId', { userId })
      .andWhere('session.active = true')
      .getMany()
      .then((users) => users.flatMap((user) => user.sessions));
  }

  async findSessionByUuidWithoutRelations(uuid: string): Promise<Session> {
    return this.sessionRepository.findOne({
      where: { uuid: uuid },
    });
  }

  async findSessionByUuid(uuid: string): Promise<Session> {
    return this.sessionRepository.findOne({
      where: { uuid: uuid },
      relations: ['propositions'],
    });
  }

  async saveUser(user: User): Promise<User> {
    return this.userRepository.save(user);
  }

  async saveClassicVote(
    vote: Vote,
    entityManager: EntityManager,
  ): Promise<Vote> {
    return entityManager.save(vote);
  }

  async saveInvestmentVote(
    votes: Vote[],
    entityManager: EntityManager,
  ): Promise<Vote[]> {
    return entityManager.save(votes);
  }

  async savePropositions(
    propositions: Proposition[],
    entityManager: EntityManager,
  ): Promise<Proposition[]> {
    return entityManager.save(propositions);
  }

  async getSessionFromPropositionId(propositionId: string): Promise<Session> {
    const proposition = await this.propositionRepository.findOne({
      where: { uuid: propositionId },
      relations: ['session', 'session.propositions'],
    });

    if (!proposition) {
      throw new Error('Proposition not found');
    }

    return proposition.session;
  }

  async addSessionToUser(
    userId: string,
    session: Session,
    entityManager: EntityManager,
  ): Promise<void> {
    const user = await this.userRepository.findOne({ where: { uuid: userId } });

    if (!user) {
      throw new Error('User not found');
    }

    await entityManager
      .createQueryBuilder()
      .relation(User, 'sessions')
      .of(user)
      .add(session);
  }
}
