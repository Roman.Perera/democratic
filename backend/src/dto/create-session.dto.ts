import {
  IsBoolean,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';

export enum SessionType {
  CLASSIC = 'classic',
  INVESTMENT = 'investment',
}

export class CreateSessionDto {
  @IsUUID()
  sessionManagerId: string;

  @IsBoolean()
  isMultiplePropositionsAllowed: boolean;

  @IsBoolean()
  isGroupSizeDefined: boolean;

  @IsNumber()
  groupSize: number;

  @IsOptional()
  @IsString()
  problematic?: string;

  @IsEnum(SessionType)
  sessionType: SessionType;
}
