import { IsUUID } from 'class-validator';

export class ClassicVoteDto {
  @IsUUID()
  propositionId: string;

  @IsUUID()
  userId: string;
}
