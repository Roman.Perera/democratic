import { Session } from 'src/entities/session.entity';

export class SessionStateUpdate {
  state: string;
  connectedUsers: number;
  nUserFinishedProposing: number;
  nUserFinishedVoting: number;

  constructor(session: Session) {
    this.state = session.state;
    this.connectedUsers = session.connectedUsers;
    this.nUserFinishedProposing = session.nUserFinishedProposing;
    this.nUserFinishedVoting = session.nUserFinishedVoting;
  }
}
