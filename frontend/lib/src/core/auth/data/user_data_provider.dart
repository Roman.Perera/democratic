import 'package:democratic/src/core/auth/data/user_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

class UserDataProvider implements UserRepository {
  static const _userKey = 'user_uuid';
  final SharedPreferences prefs;

  UserDataProvider({required this.prefs});

  @override
  Future<String> getUserUuid() async {
    String? uuid = prefs.getString(_userKey);
    if (uuid == null) {
      uuid = const Uuid().v4();
      await prefs.setString(_userKey, uuid);
    }
    return uuid;
  }
}
