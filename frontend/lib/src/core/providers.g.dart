// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'providers.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$sessionRepositoryHash() => r'7ad500da27a9bb40032547bd1c84e0af85b24f87';

/// See also [sessionRepository].
@ProviderFor(sessionRepository)
final sessionRepositoryProvider =
    AutoDisposeProvider<SessionRepository>.internal(
  sessionRepository,
  name: r'sessionRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$sessionRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef SessionRepositoryRef = AutoDisposeProviderRef<SessionRepository>;
String _$propositionRepositoryHash() =>
    r'2258162d7441550fe5869f0752e83775351b26e9';

/// See also [propositionRepository].
@ProviderFor(propositionRepository)
final propositionRepositoryProvider =
    AutoDisposeProvider<PropositionRepository>.internal(
  propositionRepository,
  name: r'propositionRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$propositionRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef PropositionRepositoryRef
    = AutoDisposeProviderRef<PropositionRepository>;
String _$voteRepositoryHash() => r'3e84ad14fc62acd46339c62bc9a7312ab5bbac39';

/// See also [voteRepository].
@ProviderFor(voteRepository)
final voteRepositoryProvider = AutoDisposeProvider<VoteRepository>.internal(
  voteRepository,
  name: r'voteRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$voteRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef VoteRepositoryRef = AutoDisposeProviderRef<VoteRepository>;
String _$userRepositoryHash() => r'8a0a821e849b61aeb26da2789b63fde63a42d1c9';

/// See also [userRepository].
@ProviderFor(userRepository)
final userRepositoryProvider =
    AutoDisposeFutureProvider<UserRepository>.internal(
  userRepository,
  name: r'userRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$userRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef UserRepositoryRef = AutoDisposeFutureProviderRef<UserRepository>;
String _$initializeHash() => r'6c4c6e5b03fa8e12bd98e568a6dce38d7c75cdf8';

/// See also [initialize].
@ProviderFor(initialize)
final initializeProvider = AutoDisposeFutureProvider<String>.internal(
  initialize,
  name: r'initializeProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$initializeHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef InitializeRef = AutoDisposeFutureProviderRef<String>;
String _$fetchUUIDHash() => r'25b71649d2f06f09b2520d6a7702ffbf084cccf6';

/// See also [fetchUUID].
@ProviderFor(fetchUUID)
final fetchUUIDProvider = FutureProvider<String>.internal(
  fetchUUID,
  name: r'fetchUUIDProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$fetchUUIDHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef FetchUUIDRef = FutureProviderRef<String>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
