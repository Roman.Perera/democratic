import 'package:democratic/src/core/auth/data/user_data_provider.dart';
import 'package:democratic/src/core/auth/data/user_mock_provider.dart';
import 'package:democratic/src/core/auth/data/user_repository.dart';
import 'package:democratic/src/core/websocket_providers.dart';
import 'package:democratic/src/features/proposition_management/data/proposition_data_provider.dart';
import 'package:democratic/src/features/proposition_management/data/proposition_mock_provider.dart';
import 'package:democratic/src/features/proposition_management/data/proposition_repository.dart';
import 'package:democratic/src/features/session_management/data/session_data_provider.dart';
import 'package:democratic/src/features/session_management/data/session_mock_provider.dart';
import 'package:democratic/src/features/session_management/data/session_repository.dart';
import 'package:democratic/src/features/voting_management/data/vote_data_provider.dart';
import 'package:democratic/src/features/voting_management/data/vote_mock_provider.dart';
import 'package:democratic/src/features/voting_management/data/vote_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
part 'providers.g.dart';

@riverpod
SessionRepository sessionRepository(SessionRepositoryRef ref) {
  return SessionDataProvider();
}

@riverpod
PropositionRepository propositionRepository(PropositionRepositoryRef ref) {
  return PropositionDataProvider();
}

@riverpod
VoteRepository voteRepository(VoteRepositoryRef ref) {
  return VoteDataProvider();
}

@riverpod
Future<UserRepository> userRepository(UserRepositoryRef ref) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return UserDataProvider(prefs: prefs);
}

@riverpod
Future<String> initialize(InitializeRef ref) async {
  final uuid = await ref.read(fetchUUIDProvider.future);
  ref.read(userIdStateProvider.notifier).state = uuid;
  ref.watch(websocketProvider(uuid));
  return uuid;
}

@Riverpod(keepAlive: true)
Future<String> fetchUUID(FetchUUIDRef ref) async {
  final userRepository = await ref.read(userRepositoryProvider.future);
  return await userRepository.getUserUuid();
}

final userIdStateProvider = StateProvider<String?>((ref) => null);
