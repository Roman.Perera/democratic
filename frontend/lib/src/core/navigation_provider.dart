import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final navigationProvider =
    StateNotifierProvider<NavigationNotifier, GlobalKey<NavigatorState>>((ref) {
  return NavigationNotifier();
});

class NavigationNotifier extends StateNotifier<GlobalKey<NavigatorState>> {
  NavigationNotifier() : super(GlobalKey<NavigatorState>());

  void navigateTo(String routeName) {
    state.currentState?.pushNamedAndRemoveUntil(routeName, (route) => false);
  }
}
