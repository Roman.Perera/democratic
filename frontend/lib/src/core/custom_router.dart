import 'package:democratic/src/core/providers.dart';
import 'package:democratic/src/features/session_management/presentation/view/home_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CustomRouter extends ConsumerWidget {
  const CustomRouter({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final initialState = ref.watch(initializeProvider);
    return switch (initialState) {
      AsyncError(:final error) => Scaffold(
          body: Center(
            child: Text('Error: $error'),
          ),
        ),
      AsyncData(:final value) => const HomePage(),
      _ => const Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        ),
    };
  }
}
