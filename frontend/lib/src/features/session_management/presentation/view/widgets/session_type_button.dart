import 'package:flutter/material.dart';

class SessionTypeButton extends StatelessWidget {
  final String text;
  final bool isSelected;
  final VoidCallback onPressed;

  const SessionTypeButton({
    Key? key,
    required this.text,
    required this.onPressed,
    this.isSelected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: onPressed,
      style: ButtonStyle(
        side: WidgetStateProperty.all(
          BorderSide(
            color: isSelected
                ? Colors.blue
                : Colors.grey[800] ?? Colors.black45, // Couleur de la bordure
            width: 2.0, // Largeur de la bordure
          ),
        ),
        backgroundColor: WidgetStateProperty.all(
          isSelected ? Colors.blue : Colors.grey[300],
        ),
        padding: WidgetStateProperty.all(
          const EdgeInsets.symmetric(vertical: 15.0),
        ),
        shape: WidgetStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
        ),
      ),
      child: Text(
        text,
        style: TextStyle(
          color: isSelected ? Colors.white : Colors.black,
          fontSize: 18,
        ),
      ),
    );
  }
}
