import 'package:democratic/src/core/navigation_provider.dart';
import 'package:democratic/src/core/providers.dart';
import 'package:democratic/src/features/session_management/domain/session.dart';
import 'package:democratic/src/features/session_management/presentation/state/session_state.dart';
import 'package:democratic/src/features/session_management/presentation/view/widgets/session_type_button.dart';
import 'package:democratic/src/shared/widgets/custom_button.dart';
import 'package:democratic/src/shared/widgets/radio_button_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CreateSessionPage extends ConsumerStatefulWidget {
  const CreateSessionPage({super.key});

  @override
  ConsumerState<CreateSessionPage> createState() => _CreateSessionPageState();
}

class _CreateSessionPageState extends ConsumerState<CreateSessionPage> {
  final _formKey = GlobalKey<FormState>();
  SessionType selectedSessionType = SessionType
      .classic; // variable pour suivre le type de session sélectionné
  bool isMultiplePropositionsAllowed = false;
  bool isGroupSizeDefined = false;
  String? problematic;
  int groupSize = 0;
  TextEditingController groupSizeController = TextEditingController();
  TextEditingController problemController = TextEditingController();

  void selectSessionType(SessionType type) {
    setState(() {
      selectedSessionType = type;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    'Paramètres de la session',
                    style: Theme.of(context).textTheme.headlineLarge!,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 20),
                  SessionTypeButton(
                    text: 'Classique',
                    isSelected: selectedSessionType == SessionType.classic,
                    onPressed: () {
                      selectSessionType(SessionType.classic);
                    },
                  ),
                  const SizedBox(height: 15),
                  Text(
                    'Un participant = un vote',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyMedium!,
                  ),
                  const SizedBox(height: 15),
                  SessionTypeButton(
                    text: 'Investissement',
                    isSelected: selectedSessionType == SessionType.investment,
                    onPressed: () {
                      selectSessionType(SessionType.investment);
                    },
                  ),
                  const SizedBox(height: 15),
                  Text(
                    'Chaque participant dispose de 100\$ à placer sur une ou plusieurs propositions.',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyMedium!,
                  ),
                  const Divider(height: 40),
                  RadioButtonTile(
                    title: 'Autoriser plusieurs propositions par personne.',
                    isSelected: isMultiplePropositionsAllowed,
                    onChanged: (value) {
                      setState(() {
                        isMultiplePropositionsAllowed = value ?? false;
                      });
                    },
                  ),
                  RadioButtonTile(
                    title: 'Taille du groupe défini',
                    subtitle:
                        '(Activé) - Les phases de session transitionnent automatiquement quand tous les participants ont terminé.\n(Désactivé) - Le chef de session sera responsable de clore les phases de session.',
                    isSelected: isGroupSizeDefined,
                    onChanged: (value) {
                      setState(() {
                        isGroupSizeDefined = value ?? false;
                      });
                    },
                  ),
                  isGroupSizeDefined
                      ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: groupSizeController,
                            decoration: const InputDecoration(
                              hintText:
                                  'Entrez la taille du groupe (vous inclus)',
                              border: OutlineInputBorder(),
                            ),
                            keyboardType: TextInputType.number,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Veuillez entrer un nombre';
                              }

                              if (int.tryParse(value) == null) {
                                return 'Veuillez entrer un nombre valide';
                              }

                              if (int.parse(value) <= 2) {
                                return 'La taille du groupe doit être supérieure à 2';
                              }

                              if (int.parse(value) > 10) {
                                return 'La taille du groupe doit être inférieure à 10';
                              }

                              return null;
                            },
                          ),
                        )
                      : const SizedBox(height: 20),
                  const Text(
                    'Problématique (optionnelle) :',
                    textAlign: TextAlign.left,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 10),
                  TextFormField(
                    controller: problemController,
                    decoration: const InputDecoration(
                      hintText: 'Lorem Ipsum...',
                      border: OutlineInputBorder(),
                    ),
                    maxLines: 5,
                  ),
                  const SizedBox(height: 20),
                  CustomButton(
                    text: 'Valider',
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        _formKey.currentState!.save();

                        if (isGroupSizeDefined) {
                          groupSize = int.parse(groupSizeController.text);
                        }
                        problematic = problemController.text;

                        bool isValid = false;

                        try {
                          final userId = ref.read(userIdStateProvider);
                          if (userId == null) {
                            throw Exception('User ID not found');
                          }
                          final sessionData = {
                            'sessionType': selectedSessionType,
                            'isMultiplePropositionsAllowed':
                                isMultiplePropositionsAllowed,
                            'isGroupSizeDefined': isGroupSizeDefined,
                            'groupSize': groupSize,
                            'sessionManagerId': userId,
                          };

                          if (problematic != null && problematic!.isNotEmpty) {
                            sessionData['problematic'] = problematic!;
                          }

                          isValid = await ref
                              .read(sessionNotifierProvider.notifier)
                              .createNewSession(sessionData);
                          if (!mounted) return;
                          if (isValid) {
                            ref
                                .read(navigationProvider.notifier)
                                .navigateTo('/create-session-success');
                          } else {
                            throw Exception('An error occurred');
                          }
                        } catch (e) {
                          print(e.toString());
                          if (!mounted) return;
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: const Text('Erreur'),
                                content: const Text(
                                    'Une erreur s\'est produite lors de la création de la session. Veuillez réessayer.'),
                                actions: [
                                  TextButton(
                                    child: const Text('OK'),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              );
                            },
                          );
                        }
                      }
                    },
                    backgroundColor:
                        Colors.grey[800], // Your preferred background color
                    textColor: Colors.white, // Your preferred text color
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
