import 'package:democratic/src/core/providers.dart';
import 'package:democratic/src/features/proposition_management/domain/proposition.dart';
import 'package:democratic/src/features/session_management/data/session_repository.dart';
import 'package:democratic/src/features/session_management/domain/session_state_info.dart';
import 'package:democratic/src/shared/state/session_state_info_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../domain/session.dart';

// const sessionStateInfo = SessionStateInfo(
//   state: SessionState.voting,
//   connectedUsers: 3,
//   nUserFinishedProposing: 3,
//   nUserFinishedVoting: 0,
// );

// List<Proposition> propositions = [
//   const Proposition(
//       id: 1,
//       text: "Pulp Fiction",
//       idSession: 1,
//       idUser: 'uuid',
//       totalAmount: 10),
//   const Proposition(
//       id: 2,
//       text: "The Dark Knight",
//       idSession: 1,
//       idUser: 'uuid',
//       totalAmount: 0),
//   const Proposition(
//       id: 3,
//       text: "The Shawshank Redemption",
//       idSession: 1,
//       idUser: 'uuid',
//       totalAmount: 0),
//   const Proposition(
//       id: 4,
//       text: "The Godfather",
//       idSession: 1,
//       idUser: 'uuid',
//       totalAmount: 0),
//   const Proposition(
//       id: 5,
//       text: "The Lord of the Rings: The Return of the King",
//       idSession: 1,
//       idUser: 'uuid',
//       totalAmount: 0),
// ];

// Session session = Session(
//     id: 'uuid',
//     pinCode: "1234",
//     multiplePropositions: true,
//     fixedSize: false,
//     size: 5,
//     stateInfo: sessionStateInfo,
//     voteType: SessionType.classic,
//     idSessionManager: "uuid",
//     propositions: propositions,
//     problematic: "Quel est le meilleur film de tous les temps ?");

final sessionNotifierProvider =
    StateNotifierProvider<SessionNotifier, Session?>((ref) {
  final sessionRepository = ref.read(sessionRepositoryProvider);
  return SessionNotifier(sessionRepository, ref);
});

class SessionNotifier extends StateNotifier<Session?> {
  final SessionRepository sessionRepository;
  final Ref ref;
  SessionNotifier(this.sessionRepository, this.ref) : super(null);

  Future<bool> createNewSession(Map<String, dynamic> session) async {
    try {
      final Session sessionWithId =
          await sessionRepository.createSession(session);
      state = sessionWithId;
      ref.read(sessionStateInfoProvider.notifier).add(sessionWithId.stateInfo);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<void> nextStep() async {
    try {
      if (state == null || state!.uuid == null) {
        throw Exception('Session is null');
      }

      await sessionRepository.nextStep(state!.uuid!);
    } catch (e) {
      throw Exception(e);
    }
  }

  void addFromMap(Map<String, dynamic> json) {
    state = Session.fromMap(json);
  }

  void updateSessionStateInfo(Map<String, dynamic> json) {
    if (state != null) {
      SessionStateInfo sessionStateInfo = SessionStateInfo.fromMap(json);
      state = state!.copyWith(stateInfo: sessionStateInfo);
    }
  }

  Future<bool> fetchSessionFromPin(String pinCode, String userId) async {
    try {
      final bool response =
          await sessionRepository.fetchSessionFromPin(pinCode, userId);

      return response;
    } catch (e) {
      return false;
    }
  }
}
