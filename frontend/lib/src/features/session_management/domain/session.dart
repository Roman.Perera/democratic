import 'dart:convert';

import 'package:democratic/src/features/proposition_management/domain/proposition.dart';
import 'package:democratic/src/features/session_management/domain/session_state_info.dart';
import 'package:flutter/material.dart';

enum SessionType {
  classic,
  investment,
}

extension SessionTypeExtension on SessionType {
  String toShortString() {
    return toString().split('.').last;
  }

  static SessionType fromString(String type) {
    switch (type) {
      case 'classic':
        return SessionType.classic;
      case 'investment':
        return SessionType.investment;
      default:
        throw Exception('Unknown session type');
    }
  }
}

@immutable
class Session {
  final String? uuid;
  final String pinCode;
  final bool multiplePropositions;
  final bool fixedSize;
  final int size;
  final List<Proposition> propositions;
  final SessionStateInfo stateInfo;
  final SessionType voteType;
  final String? problematic;
  final String idSessionManager;

  const Session({
    this.uuid,
    required this.pinCode,
    required this.multiplePropositions,
    required this.fixedSize,
    required this.size,
    required this.stateInfo,
    required this.voteType,
    this.problematic,
    this.propositions = const [],
    required this.idSessionManager,
  });

  factory Session.fromMap(Map<String, dynamic> json) => Session(
        uuid: json['uuid'],
        pinCode: json['pinCode'],
        multiplePropositions: json['multiplePropositions'],
        problematic: json['problematic'],
        voteType: SessionTypeExtension.fromString(json['type']),
        fixedSize: json['fixedSize'],
        size: json['size'],
        idSessionManager: json['sessionManager']['uuid'],
        propositions:
            (json['propositions'] != null && json['propositions'].isNotEmpty)
                ? List<Proposition>.from(json['propositions']
                    .map((proposition) => Proposition.fromMap(proposition)))
                : [],
        stateInfo: SessionStateInfo.fromMap(json),
      );

  Map<String, dynamic> toMap() => {
        'uuid': uuid,
        'pinCode': pinCode,
        'multiplePropositions': multiplePropositions,
        'fixedSize': fixedSize,
        'size': size,
        'state': stateInfo.state,
        'connectedUsers': stateInfo.connectedUsers,
        'nUserFinishedProposing': stateInfo.nUserFinishedProposing,
        'nUserFinishedVoting': stateInfo.nUserFinishedVoting,
        'type': voteType.toShortString(),
        'problematic': problematic,
        'propositions':
            propositions.map((proposition) => proposition.toMap()).toList(),
        'sessionManager': {'uuid': idSessionManager},
      };

  Session copyWith({
    String? id,
    String? pinCode,
    bool? multipleProposition,
    bool? fixedSize,
    int? size,
    SessionStateInfo? stateInfo,
    SessionType? voteType,
    String? problematic,
    List<Proposition>? propositions,
    String? idSessionManager,
  }) =>
      Session(
        uuid: id ?? this.uuid,
        pinCode: pinCode ?? this.pinCode,
        multiplePropositions: multipleProposition ?? this.multiplePropositions,
        fixedSize: fixedSize ?? this.fixedSize,
        size: size ?? this.size,
        stateInfo: stateInfo ?? this.stateInfo,
        voteType: voteType ?? this.voteType,
        problematic: problematic ?? this.problematic,
        propositions: propositions ?? this.propositions,
        idSessionManager: idSessionManager ?? this.idSessionManager,
      );

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Session &&
          runtimeType == other.runtimeType &&
          uuid == other.uuid &&
          pinCode == other.pinCode &&
          multiplePropositions == other.multiplePropositions &&
          fixedSize == other.fixedSize &&
          size == other.size &&
          stateInfo == other.stateInfo &&
          voteType == other.voteType &&
          problematic == other.problematic &&
          propositions == other.propositions &&
          idSessionManager == other.idSessionManager;

  @override
  int get hashCode =>
      uuid.hashCode ^
      pinCode.hashCode ^
      multiplePropositions.hashCode ^
      fixedSize.hashCode ^
      size.hashCode ^
      stateInfo.hashCode ^
      voteType.hashCode ^
      problematic.hashCode ^
      propositions.hashCode ^
      idSessionManager.hashCode;

  @override
  String toString() {
    return 'Session{id: $uuid, pinCode: $pinCode, multipleVote: $multiplePropositions, fixedSize: $fixedSize, size: $size, state: $stateInfo, voteType: $voteType, problematic: $problematic, idSessionManager: $idSessionManager}';
  }

  static Session fromJson(String json) {
    return Session.fromMap(jsonDecode(json));
  }

  String toJson() {
    return jsonEncode(toMap());
  }
}
