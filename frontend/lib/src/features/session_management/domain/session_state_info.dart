import 'package:flutter/material.dart';

enum SessionState {
  proposing,
  voting,
  cancelled,
  finished,
}

extension SessionStateExtension on SessionState {
  String toShortString() {
    return toString().split('.').last;
  }

  static SessionState fromString(String type) {
    switch (type) {
      case 'proposing':
        return SessionState.proposing;
      case 'voting':
        return SessionState.voting;
      case 'cancelled':
        return SessionState.cancelled;
      case 'finished':
        return SessionState.finished;
      default:
        throw Exception('Unknown session state');
    }
  }
}

@immutable
class SessionStateInfo {
  final SessionState state;
  final int connectedUsers;
  final int nUserFinishedProposing;
  final int nUserFinishedVoting;

  const SessionStateInfo({
    required this.state,
    this.connectedUsers = 1,
    this.nUserFinishedProposing = 0,
    this.nUserFinishedVoting = 0,
  });

  factory SessionStateInfo.fromMap(Map<String, dynamic> json) =>
      SessionStateInfo(
        state: SessionStateExtension.fromString(json['state']),
        connectedUsers: json['connectedUsers'],
        nUserFinishedProposing: json['nUserFinishedProposing'],
        nUserFinishedVoting: json['nUserFinishedVoting'],
      );

  Map<String, dynamic> toMap() => {
        'state': state,
        'connectedUsers': connectedUsers,
        'nUserFinishedProposing': nUserFinishedProposing,
        'nUserFinishedVoting': nUserFinishedVoting,
      };

  SessionStateInfo copyWith({
    SessionState? state,
    int? connectedUsers,
    int? nUserFinishedProposing,
    int? nUserFinishedVoting,
  }) {
    return SessionStateInfo(
      state: state ?? this.state,
      connectedUsers: connectedUsers ?? this.connectedUsers,
      nUserFinishedProposing:
          nUserFinishedProposing ?? this.nUserFinishedProposing,
      nUserFinishedVoting: nUserFinishedVoting ?? this.nUserFinishedVoting,
    );
  }

  SessionStateInfo copyWithFromMap(Map<String, dynamic> json) {
    return SessionStateInfo(
      state: (json['state'] != null)
          ? SessionStateExtension.fromString(json['state'])
          : state,
      connectedUsers: json['connectedUsers'] ?? connectedUsers,
      nUserFinishedProposing:
          json['nUserFinishedProposing'] ?? nUserFinishedProposing,
      nUserFinishedVoting: json['nUserFinishedVoting'] ?? nUserFinishedVoting,
    );
  }

  @override
  String toString() {
    return 'SessionStateInfo(state: $state, connectedUsers: $connectedUsers, nUserFinishedProposing: $nUserFinishedProposing, nUserFinishedVoting: $nUserFinishedVoting)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SessionStateInfo &&
        other.state == state &&
        other.connectedUsers == connectedUsers &&
        other.nUserFinishedProposing == nUserFinishedProposing &&
        other.nUserFinishedVoting == nUserFinishedVoting;
  }

  @override
  int get hashCode {
    return state.hashCode ^
        connectedUsers.hashCode ^
        nUserFinishedProposing.hashCode ^
        nUserFinishedVoting.hashCode;
  }
}
