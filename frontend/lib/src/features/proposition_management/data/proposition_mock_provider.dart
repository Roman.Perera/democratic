import 'package:democratic/src/features/proposition_management/data/proposition_repository.dart';
import 'package:democratic/src/features/proposition_management/domain/proposition.dart';

class PropositionMockProvider implements PropositionRepository {
  @override
  Future<void> createPropositions(
      String uuid, String sessionId, List<String> propositions) async {}
}
