import 'package:democratic/src/features/proposition_management/domain/proposition.dart';

abstract class PropositionRepository {
  Future<void> createPropositions(
      String uuid, String sessionId, List<String> propositions);
}
