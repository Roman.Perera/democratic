import 'package:democratic/src/core/providers.dart';
import 'package:democratic/src/features/proposition_management/data/proposition_repository.dart';
import 'package:democratic/src/features/proposition_management/domain/proposition.dart';
import 'package:flutter/material.dart';
import 'package:riverpod/riverpod.dart';

final myPropositionsProvider =
    StateNotifierProvider<MyPropositionsNotifier, void>((ref) {
  final propositionRepository = ref.read(propositionRepositoryProvider);
  return MyPropositionsNotifier(propositionRepository, ref);
});

class MyPropositionsNotifier extends StateNotifier<void> {
  final PropositionRepository propositionRepository;
  final Ref ref;
  MyPropositionsNotifier(this.propositionRepository, this.ref) : super(null);

  Future<void> createNewPropositions(
      String sessionId, List<String> propositions) async {
    try {
      final uuid = ref.read(userIdStateProvider);

      if (uuid == null) {
        throw Exception('UUID not found');
      }

      await propositionRepository.createPropositions(
          uuid, sessionId, propositions);
    } catch (e) {
      throw Exception(e);
    }
  }
}

class PropositionsNotifier extends StateNotifier<List<TextEditingController>> {
  PropositionsNotifier() : super([TextEditingController()]);

  void addProposition() {
    state = [...state, TextEditingController()];
  }

  void removeProposition(int index) {
    state[index].dispose();
    state = [
      for (int i = 0; i < state.length; i++)
        if (i != index) state[i]
    ];
  }

  void updateProposition(int index) {
    state = [...state];
  }

  void disposeControllers() {
    for (var controller in state) {
      controller.dispose();
    }

    state = [TextEditingController()];
  }
}

final propositionsProvider =
    StateNotifierProvider<PropositionsNotifier, List<TextEditingController>>(
        (ref) => PropositionsNotifier());
