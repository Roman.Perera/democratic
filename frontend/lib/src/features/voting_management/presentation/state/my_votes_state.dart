import 'package:democratic/src/core/providers.dart';
import 'package:democratic/src/features/voting_management/data/vote_repository.dart';
import 'package:democratic/src/features/voting_management/domain/vote.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final myVotesProvider =
    StateNotifierProvider<MyVotesNotifier, List<Vote>>((ref) {
  final voteRepository = ref.read(voteRepositoryProvider);
  return MyVotesNotifier(voteRepository);
});

class MyVotesNotifier extends StateNotifier<List<Vote>> {
  final VoteRepository voteRepository;
  MyVotesNotifier(this.voteRepository) : super([]);

  void addVote(Vote vote) {
    state = [...state, vote];
  }

  void removeVote(Vote vote) {
    state = state.where((element) => element != vote).toList();
  }

  Future<void> sendVotes(List<Map<String, dynamic>> votes) async {
    await voteRepository.investmentVote(votes);
    state = [];
  }
}
