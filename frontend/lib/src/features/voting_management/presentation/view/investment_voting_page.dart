import 'package:democratic/src/core/navigation_provider.dart';
import 'package:democratic/src/core/providers.dart';
import 'package:democratic/src/features/proposition_management/domain/proposition.dart';
import 'package:democratic/src/features/session_management/domain/session.dart';
import 'package:democratic/src/features/session_management/domain/session_state_info.dart';
import 'package:democratic/src/features/session_management/presentation/state/session_state.dart';
import 'package:democratic/src/features/voting_management/domain/vote.dart';
import 'package:democratic/src/features/voting_management/presentation/state/my_votes_state.dart';
import 'package:democratic/src/shared/state/session_state_info_provider.dart';
import 'package:democratic/src/shared/widgets/app_bar.dart';
import 'package:democratic/src/shared/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class InvestmentVotePanel {
  bool isExpanded = false;
  final Proposition proposition;
  int currentAmount = 0;

  InvestmentVotePanel(this.proposition);
}

List<InvestmentVotePanel> generatePanelList(List<Proposition> propositions) {
  return propositions
      .map((proposition) => InvestmentVotePanel(proposition))
      .toList();
}

class InvestmentVotingPage extends ConsumerStatefulWidget {
  const InvestmentVotingPage({super.key});

  @override
  ConsumerState<InvestmentVotingPage> createState() =>
      _InvestmentVotingPageState();
}

class _InvestmentVotingPageState extends ConsumerState<InvestmentVotingPage> {
  int _totalAmountLeft = 100;
  int _totalAmountInvested = 0;
  final List<InvestmentVotePanel> _panels = [];

  @override
  void initState() {
    super.initState();
    final session = ref.read(sessionNotifierProvider);

    if (session == null || session.propositions.isEmpty) {
      return;
    }

    _panels.addAll(generatePanelList(session.propositions));
  }

  @override
  Widget build(BuildContext context) {
    final uuid = ref.read(userIdStateProvider);
    final session = ref.read(sessionNotifierProvider);

    if (session == null || uuid == null) {
      return const Scaffold(
        body: Center(
          child: Text('Erreur, veuillez relancer l\'application.'),
        ),
      );
    }

    return SafeArea(
      child: Scaffold(
        appBar: const CustomAppBar(),
        body: Center(
          child: SingleChildScrollView(
            padding:
                const EdgeInsets.symmetric(horizontal: 16.0, vertical: 40.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(
                  (session.problematic != null)
                      ? session.problematic!
                      : 'Quel est la meilleure proposition ?',
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 20),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: 'Il vous reste ',
                    style: const TextStyle(fontSize: 16, color: Colors.black),
                    children: <TextSpan>[
                      TextSpan(
                        text: '$_totalAmountLeft 💵',
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.green,
                        ),
                      ),
                      const TextSpan(
                        text: ' à investir',
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                ExpansionPanelList(
                  elevation: 1,
                  expandedHeaderPadding: EdgeInsets.zero,
                  expansionCallback: (int index, bool isExpanded) {
                    setState(() {
                      _panels[index].isExpanded = isExpanded;
                    });
                  },
                  children:
                      _panels.map<ExpansionPanel>((InvestmentVotePanel panel) {
                    return ExpansionPanel(
                      headerBuilder: (BuildContext context, bool isExpanded) {
                        return ListTile(
                          title: Text(panel.proposition.text),
                        );
                      },
                      body: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: Slider(
                                value: panel.currentAmount.toDouble(),
                                min: 0,
                                max: 100,
                                onChanged: (double value) {
                                  setState(() {
                                    if (value.toInt() >=
                                        _totalAmountLeft +
                                            panel.currentAmount) {
                                      panel.currentAmount = _totalAmountLeft +
                                          panel.currentAmount;
                                    } else {
                                      panel.currentAmount = value.toInt();
                                    }
                                    _totalAmountInvested = _panels
                                        .map((panel) => panel.currentAmount)
                                        .reduce((a, b) => a + b);
                                    _totalAmountLeft =
                                        100 - _totalAmountInvested;
                                  });
                                },
                              ),
                            ),
                            Text('${panel.currentAmount} 💵'),
                          ],
                        ),
                      ),
                      isExpanded: panel.isExpanded,
                    );
                  }).toList(),
                ),
                const SizedBox(height: 20),
                CustomButton(
                  text: 'Valider mon vote',
                  onPressed: () async {
                    try {
                      List<Map<String, dynamic>> votes = _panels
                          .map((panel) => {
                                'userId': uuid,
                                'propositionId': panel.proposition.uuid,
                                'amount': panel.currentAmount,
                              })
                          .toList();
                      await ref
                          .read(voteRepositoryProvider)
                          .investmentVote(votes);
                      if (uuid == session.idSessionManager &&
                          !session.fixedSize) {
                        final stateInfo = ref.read(sessionStateInfoProvider);
                        if (stateInfo == null) {
                          ref
                              .read(sessionStateInfoProvider.notifier)
                              .add(session.stateInfo);
                        }
                        ref
                            .read(navigationProvider.notifier)
                            .navigateTo('/manager-monitoring-page');
                      } else {
                        ref
                            .read(navigationProvider.notifier)
                            .navigateTo('/waiting-page');
                      }
                    } catch (e) {
                      print(e);
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
