import 'package:democratic/src/core/navigation_provider.dart';
import 'package:democratic/src/features/session_management/presentation/state/session_state.dart';
import 'package:democratic/src/features/voting_management/presentation/view/widgets/proposition_result_tile.dart';
import 'package:democratic/src/shared/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class VotingResultsPage extends ConsumerWidget {
  const VotingResultsPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final session = ref.watch(sessionNotifierProvider);
    if (session == null) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }

    int totalVotes = session.propositions.fold(
        0,
        (previousValue, proposition) =>
            previousValue + proposition.totalAmount);

    return SafeArea(
      child: Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                for (var proposition in session.propositions)
                  ResultTile(
                    proposition: proposition.text,
                    votes: proposition.totalAmount,
                    totalVotes: totalVotes,
                    sessionType: session.voteType,
                  ),
                SizedBox(height: 30),
                CustomButton(
                    text: 'Retour',
                    onPressed: () {
                      ref.read(navigationProvider.notifier).navigateTo("/");
                    })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
