import 'package:democratic/src/core/navigation_provider.dart';
import 'package:democratic/src/core/providers.dart';
import 'package:democratic/src/features/proposition_management/domain/proposition.dart';
import 'package:democratic/src/features/session_management/domain/session.dart';
import 'package:democratic/src/features/session_management/presentation/state/session_state.dart';
import 'package:democratic/src/features/session_management/presentation/view/widgets/session_type_button.dart';
import 'package:democratic/src/features/session_management/domain/session_state_info.dart';
import 'package:democratic/src/shared/state/session_state_info_provider.dart';
import 'package:democratic/src/shared/widgets/app_bar.dart';
import 'package:democratic/src/shared/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ClassicVotingPage extends ConsumerStatefulWidget {
  const ClassicVotingPage({super.key});

  @override
  ConsumerState<ClassicVotingPage> createState() => _ClassicVotePageState();
}

class _ClassicVotePageState extends ConsumerState<ClassicVotingPage> {
  Proposition? selectedProposition;

  @override
  Widget build(BuildContext context) {
    final session = ref.watch(sessionNotifierProvider);

    if (session == null) {
      return const Scaffold(
        body: Center(
          child: Text('Erreur, veuillez relancer l\'application.'),
        ),
      );
    }

    return SafeArea(
        child: Scaffold(
            appBar: const CustomAppBar(),
            body: Center(
              child: SingleChildScrollView(
                padding: const EdgeInsets.symmetric(
                    horizontal: 16.0, vertical: 40.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                        (session.problematic != null)
                            ? session.problematic!
                            : 'Quel est la meilleure proposition ?',
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                        textAlign: TextAlign.center),
                    const SizedBox(height: 20),
                    for (final proposition in session.propositions)
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SessionTypeButton(
                          text: proposition.text,
                          onPressed: () {
                            setState(() {
                              selectedProposition = proposition;
                            });
                          },
                          isSelected: selectedProposition == proposition,
                        ),
                      ),
                    const SizedBox(height: 20),
                    CustomButton(
                        text: 'Valider mon vote',
                        onPressed: () async {
                          try {
                            if (selectedProposition != null &&
                                selectedProposition!.uuid != null) {
                              final uuid = ref.read(userIdStateProvider);
                              if (uuid != null) {
                                await ref
                                    .read(voteRepositoryProvider)
                                    .classicVote(
                                        uuid, selectedProposition!.uuid!);
                                if (uuid == session.idSessionManager &&
                                    !session.fixedSize) {
                                  ref
                                      .read(sessionStateInfoProvider.notifier)
                                      .add(session.stateInfo);
                                  ref
                                      .read(navigationProvider.notifier)
                                      .navigateTo('/manager-monitoring-page');
                                } else {
                                  ref
                                      .read(navigationProvider.notifier)
                                      .navigateTo('/waiting-page');
                                }
                              } else {
                                throw Exception('No userId found');
                              }
                            } else {
                              throw Exception('No proposition selected');
                            }
                          } catch (e) {
                            if (!mounted) return;
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: const Text('Erreur'),
                                  content: Text(
                                      'Une erreur s\'est produite.\nVérifiez votre connexion internet et réessayez.\nSi le problème persiste, relancez l\'application.\n\n$e'),
                                  actions: [
                                    TextButton(
                                      child: const Text('OK'),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ],
                                );
                              },
                            );
                          }
                        })
                  ],
                ),
              ),
            )));
  }
}
