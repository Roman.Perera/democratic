import 'package:democratic/src/features/session_management/domain/session.dart';
import 'package:flutter/material.dart';

class ResultTile extends StatelessWidget {
  final String proposition;
  final int votes;
  final int totalVotes;
  final SessionType sessionType;

  const ResultTile({
    Key? key,
    required this.proposition,
    required this.votes,
    required this.totalVotes,
    required this.sessionType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double votePercentage = totalVotes > 0 ? (votes / totalVotes) : 0.0;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            proposition,
            style: const TextStyle(
              fontSize: 16,
              fontStyle: FontStyle.italic,
            ),
          ),
          const SizedBox(height: 8),
          Stack(
            children: [
              Container(
                height: 30,
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(color: Colors.black),
                ),
              ),
              FractionallySizedBox(
                widthFactor: votePercentage,
                child: Container(
                  height: 30,
                  decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.black),
                  ),
                ),
              ),
              Positioned.fill(
                child: Center(
                  child: Text(
                    '$votes votes',
                    style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
