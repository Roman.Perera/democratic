import 'dart:convert';

import 'package:democratic/src/core/constants.dart';
import 'package:democratic/src/features/voting_management/data/vote_repository.dart';
import 'package:democratic/src/features/voting_management/domain/vote.dart';
import 'package:http/http.dart' as http;

class VoteDataProvider implements VoteRepository {
  @override
  Future<void> classicVote(String userId, String idProposition) async {
    try {
      print('userId: $userId, idProposition: $idProposition');
      final response = await http.post(
        Uri.parse('$baseUrl/vote/classic'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({'userId': userId, 'propositionId': idProposition}),
      );
      print(response.body);
      if (response.statusCode != 201) {
        throw Exception('Failed to vote');
      }
    } catch (e) {
      throw Exception(e);
    }
  }

  @override
  Future<void> investmentVote(List<Map<String, dynamic>> votes) async {
    try {
      final response = await http.post(
        Uri.parse('$baseUrl/vote/investment'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({'votes': votes}),
      );
      if (response.statusCode != 201) {
        throw Exception('Failed to vote');
      }
    } catch (e) {
      throw Exception(e);
    }
  }
}
