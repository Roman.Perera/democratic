import 'package:democratic/src/features/voting_management/domain/vote.dart';

abstract class VoteRepository {
  Future<void> classicVote(String uuid, String idProposition);
  Future<void> investmentVote(List<Map<String, dynamic>> votes);
}
