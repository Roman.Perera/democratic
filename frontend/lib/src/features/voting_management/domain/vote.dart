import 'dart:convert';

import 'package:flutter/material.dart';

@immutable
class Vote {
  final int? id;
  final String? userId;
  final String propositionId;
  final int amount;

  const Vote({
    this.id,
    this.userId,
    required this.propositionId,
    required this.amount,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Vote &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          userId == other.userId &&
          propositionId == other.propositionId &&
          amount == other.amount;

  @override
  int get hashCode =>
      id.hashCode ^ userId.hashCode ^ propositionId.hashCode ^ amount.hashCode;

  @override
  String toString() {
    return 'Vote{id: $id, idUser: $userId, idProposition: $propositionId, amount: $amount}';
  }

  Vote copyWith({
    int? id,
    String? idUser,
    String? idProposition,
    int? amount,
  }) {
    return Vote(
      id: id ?? this.id,
      userId: idUser ?? this.userId,
      propositionId: idProposition ?? this.propositionId,
      amount: amount ?? this.amount,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'idUser': userId,
      'idProposition': propositionId,
      'amount': amount,
    };
  }

  factory Vote.fromMap(Map<String, dynamic> map) {
    return Vote(
      id: map['id'] as int?,
      userId: map['idUser'] as String,
      propositionId: map['idProposition'] as String,
      amount: map['amount'] as int,
    );
  }

  String toJson() => jsonEncode(toMap());

  factory Vote.fromJson(String source) =>
      Vote.fromMap(jsonDecode(source) as Map<String, dynamic>);
}
