import 'package:democratic/src/features/session_management/domain/session_state_info.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final sessionStateInfoProvider =
    StateNotifierProvider<SessionStateInfoNotifier, SessionStateInfo?>((ref) {
  return SessionStateInfoNotifier();
});

class SessionStateInfoNotifier extends StateNotifier<SessionStateInfo?> {
  SessionStateInfoNotifier() : super(null);

  void addFromMap(Map<String, dynamic> json) {
    state = SessionStateInfo.fromMap(json);
  }

  void add(SessionStateInfo sessionStateInfo) {
    state = sessionStateInfo;
  }

  void updateFromMap(Map<String, dynamic> json) {
    if (state == null) {
      return;
    }
    state = state!.copyWithFromMap(json);
  }

  void updateState(SessionState state) {
    if (this.state == null) {
      return;
    }
    this.state = this.state!.copyWith(state: state);
  }

  void updateConnectedUsers(int connectedUsers) {
    if (state == null) {
      return;
    }
    state = state!.copyWith(connectedUsers: connectedUsers);
  }

  void updateNUserFinishedProposing(int nUserFinishedProposing) {
    if (state == null) {
      return;
    }
    state = state!.copyWith(nUserFinishedProposing: nUserFinishedProposing);
  }

  void updateNUserFinishedVoting(int nUserFinishedVoting) {
    if (state == null) {
      return;
    }
    state = state!.copyWith(nUserFinishedVoting: nUserFinishedVoting);
  }

  void resetState() {
    state = null;
  }
}
