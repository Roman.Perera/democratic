import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String? title;
  final String? pinCode;
  final List<Widget>? actions;

  const CustomAppBar({Key? key, this.title, this.pinCode, this.actions})
      : preferredSize = const Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  final Size preferredSize; // default is 56.0

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        title ?? ((pinCode != null) ? 'Session PIN code : $pinCode' : ''),
        style: const TextStyle(color: Colors.black),
      ),
      actions: actions,
      centerTitle: true,
      backgroundColor: Colors.white,
    );
  }
}
