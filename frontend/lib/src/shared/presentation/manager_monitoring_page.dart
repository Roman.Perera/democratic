import 'package:democratic/src/features/session_management/domain/session.dart';
import 'package:democratic/src/features/session_management/presentation/state/session_state.dart';
import 'package:democratic/src/features/session_management/domain/session_state_info.dart';
import 'package:democratic/src/shared/state/session_state_info_provider.dart';
import 'package:democratic/src/shared/widgets/app_bar.dart';
import 'package:democratic/src/shared/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ManagerMonitoringPage extends ConsumerStatefulWidget {
  const ManagerMonitoringPage({super.key});

  @override
  ConsumerState<ManagerMonitoringPage> createState() =>
      _ManagerMonitoringPageState();
}

class _ManagerMonitoringPageState extends ConsumerState<ManagerMonitoringPage> {
  @override
  Widget build(BuildContext context) {
    final session = ref.watch(sessionNotifierProvider);
    final sessionStateInfo = ref.watch(sessionStateInfoProvider);

    if (session == null || sessionStateInfo == null) {
      return const Scaffold(
        body: Center(
          child: Text('Erreur, veuillez relancer l\'application.'),
        ),
      );
    }

    int nUserFinished;
    int nConnectedUsers;
    String buttonText = sessionStateInfo.state == SessionState.proposing
        ? 'Passer à la phase de vote'
        : 'Passer au résultats';

    if (sessionStateInfo.state == SessionState.proposing) {
      nUserFinished = sessionStateInfo.nUserFinishedProposing - 1;
    } else {
      nUserFinished = sessionStateInfo.nUserFinishedVoting - 1;
    }
    nConnectedUsers = sessionStateInfo.connectedUsers - 1;

    return SafeArea(
      child: Scaffold(
          appBar: CustomAppBar(
            pinCode: session.pinCode,
          ),
          body: Center(
            child: sessionStateInfo.connectedUsers > 1
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      RichText(
                        text: TextSpan(
                          text: 'Il y a actuellement ',
                          children: <TextSpan>[
                            TextSpan(
                              text:
                                  '$nConnectedUsers ${nConnectedUsers == 1 ? 'autre' : 'autres'}',
                              style:
                                  const TextStyle(fontWeight: FontWeight.bold),
                            ),
                            TextSpan(
                                text:
                                    ' ${nConnectedUsers == 1 ? 'participant' : 'participants'} dans la session.'),
                          ],
                        ),
                      ),
                      const SizedBox(height: 20),
                      RichText(
                        text: TextSpan(
                          text: '$nUserFinished sur $nConnectedUsers',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: nUserFinished == nConnectedUsers
                                ? Colors.green
                                : Colors.red,
                          ),
                          children: <TextSpan>[
                            TextSpan(
                              text:
                                  ' ont terminé de ${sessionStateInfo.state == SessionState.proposing ? 'proposer' : 'voter'}.',
                              style: const TextStyle(
                                fontWeight: FontWeight.normal,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 20),
                      (nConnectedUsers > 1)
                          ? CustomButton(
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: const Text('Confirmation'),
                                      content: const Text(
                                          'Etes vous sûr de bien vouloir procéder à l\'étape suivante ?'),
                                      actions: [
                                        TextButton(
                                          onPressed: () async {
                                            Navigator.of(context).pop();
                                            try {
                                              await ref
                                                  .read(sessionNotifierProvider
                                                      .notifier)
                                                  .nextStep();
                                              if (!mounted) return;
                                            } catch (e) {
                                              if (!mounted) return;
                                              showDialog(
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return AlertDialog(
                                                    title: const Text('Erreur'),
                                                    content: Text(
                                                        'Une erreur s\'est produite.\nVérifiez votre connexion internet et réessayez.\nSi le problème persiste, relancez l\'application.\n\n$e'),
                                                    actions: [
                                                      TextButton(
                                                        child: const Text('OK'),
                                                        onPressed: () {
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                      ),
                                                    ],
                                                  );
                                                },
                                              );
                                            }
                                          },
                                          child: const Text('Confirmer'),
                                        ),
                                        TextButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: const Text('Annuler'),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                              text: buttonText,
                            )
                          : CustomButton(
                              text: buttonText,
                              onPressed: () {},
                              active: false,
                            ),
                    ],
                  )
                : const Text(
                    'Vous êtes seul dans la session.\n Partagez le code PIN pour inviter des participants.'),
          )),
    );
  }
}
