import 'package:democratic/src/features/session_management/domain/session.dart';
import 'package:democratic/src/features/session_management/presentation/state/session_state.dart';
import 'package:democratic/src/features/session_management/domain/session_state_info.dart';
import 'package:democratic/src/shared/widgets/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class WaitingPage extends ConsumerWidget {
  const WaitingPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final session = ref.watch(sessionNotifierProvider);
    final title = session != null
        ? (session.stateInfo.state == SessionState.proposing)
            ? 'Vous passerez bientôt à la phase de vote, veuillez patienter...'
            : 'Les résultats du vote seront disponible lorsque tout le monde aura participé.'
        : 'Chargement...';
    return Scaffold(
      appBar:
          (session != null && session.stateInfo.state == SessionState.proposing)
              ? CustomAppBar(
                  pinCode: session.pinCode,
                )
              : null,
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const CircularProgressIndicator(),
              const SizedBox(height: 20),
              Text(title),
            ],
          ),
        ),
      ),
    );
  }
}
