import 'package:democratic/src/core/app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  runApp(
    const ProviderScope(
      child: MyApp(),
    ),
  );
}

/*TODO List: 
- Ajouter une page d'erreur et envoyer un POST à l'API pour logger l'erreur
*/
